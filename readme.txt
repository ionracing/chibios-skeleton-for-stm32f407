*****************************************************************************
** ChibiOS skeleton project			                             **
*****************************************************************************

** AUTOHOR **
Aleksander K. Ferkingstad

** PLATFORM **
	STM32F407VGT6
** BUILD PROCESS **
	In terminal: go to the directory of the makefile.

	run the command: 
	> make
	This will generate the folder build.

	Then go to the build folder.
	> ls build
	Now you are in the directory of the elf file. 
	You can now start the upload process.

** UPPLOAD PROCESS **
	In terminal:
	Run “./st-util” note the port it opens.
	in a new terminal window run “arm-none-eabi-gcc”
	This will open a GDB connection.
	Tell the GDB connection to connect to the microcontroller:
	tar ext: “port”
	the standard port value is 4242.
	load projectName.elf

** FUNCTIONALITY ** 
Basic skeleton project with a heart-beat LED for verification purposes.